// import "./modules/general";
// import "./modules/stickyMenu";
// import "./modules/mobileMenu";

window.addEventListener('scroll', () => {
    const paper = document.getElementsByClassName('paper')[0]
    paper.style.webkitAnimationPlayState = "paused";

    const envelopeBack = document.getElementsByClassName('back')[0]
    const envelopeFront = document.getElementsByClassName('front')[0]
    envelopeBack.style.marginBottom = "-140px";
    envelopeFront.style.marginBottom = "-140px";
})