import $ from "jquery";

const $body = $('body');
const $window = $(window);

export {
  $body,
  $window
}